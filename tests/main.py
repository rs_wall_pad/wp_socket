import asyncio
import logging

from wp_socket.client import WpSocketClient

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    async def _async_packets_handler(packets):
        logging.debug(packets)


    loop = asyncio.get_event_loop()
    sock = WpSocketClient("192.168.100.30", 8899)
    sock.async_receive_handler = _async_packets_handler
    loop.create_task(sock.async_connect())
    loop.run_forever()
